import pandas as pd


def work_in_excel(path):
    count = 0
    file = pd.ExcelFile(path)
    open_file = pd.DataFrame(pd.read_excel(file))
    item_types = {}
    items = []
    regions = {}
    prioritys = {}

    for index in open_file.index:
        count += 1
        i_type = open_file['Item Type'][index]
        region = open_file['Region'][index]
        priority = open_file['Order Priority'][index]

        if i_type in item_types.keys():
            item_types[i_type] += 1
        else:
            item_types[i_type] = 1

        items = list(item_types.keys())

        if region in regions.keys():
            regions[region] += 1
        else:
            regions[region] = 1

        if priority in prioritys.keys():
            prioritys[priority] += 1
        else:
            prioritys[priority] = 1

    return {'row_count': count, 'items': items, 'regions_and_count': regions,
            'orders': prioritys, 'orders_by_item_type': item_types}