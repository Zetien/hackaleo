def num_float(number):
    for letter in number:
        if letter == ",":
            number = number.replace(letter, "")

    return float(number)
