import datetime


def Date_xls(date):
    date = str(date)
    n = date.split("/")
    date_f = datetime.datetime(
        int(n[2]), int(n[0]), int(n[1])
    ).date()
    return date_f

# print(Date_xls('01/05/2020'))
