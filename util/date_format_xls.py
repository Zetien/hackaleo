from pprint import pprint

import pandas as pd
from .entero import num_int
from .format_xls import Date_xls


def filter_data(path):
    datos = []
    open_file = pd.DataFrame(pd.read_excel(path))
    for i in open_file.index:
        reg = {}
        new_region = open_file['Region'][i]
        new_contry = open_file['Country'][i]
        new_item_type = open_file['Item Type'][i]
        new_sales_channel = open_file['Sales Channel'][i]
        new_order_priority = open_file['Order Priority'][i]
        new_order_date = open_file['Order Date'][i]
        new_order_id = open_file['Order ID'][i]
        new_ship_date = open_file['Ship Date'][i]
        new_units_sold = num_int(open_file['Units Sold'][i])
        new_unit_price = num_int(open_file['Unit Price'][i])
        new_unit_cost = num_int(open_file['Unit Cost'][i])
        new_total_revenue = num_int(open_file['Total Revenue'][i])
        new_total_cost = num_int(open_file['Total Cost'][i])
        new_total_profit = num_int(open_file['Total Profit'][i])

        if '/' in str(new_ship_date) and '/' in str(new_order_date):
            new_order_date = Date_xls(new_order_date)
            new_ship_date = Date_xls(new_ship_date)
            new_order_id = num_int(new_order_id)
            reg = {
                'Region': new_region,
                'Country': new_contry,
                'Item Type': new_item_type,
                'Sales Channel': new_sales_channel,
                'Order Priority': new_order_priority,
                'Order Date': new_order_date,
                'Order ID': new_order_id,
                'Ship Date': new_ship_date,
                'Units Sold': new_units_sold,
                'Unit Price': new_unit_price,
                'Unit Cost': new_unit_cost,
                'Total Revenue': new_total_revenue,
                'Total Cost': new_total_cost,
                'Total Profit': new_total_profit,
            }
            datos.append(reg)
    return datos


#print(filter_data("/home/lsv/Documentos/lsv/Django/hack_django/hackaton_django/util/Sales.xlsx"))
