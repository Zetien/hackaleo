from django.urls import path

from read_file.views import Upload_csv, Upload_xls

urlpatterns = [
    path("upload/", Upload_csv, name="upload"),
    path('upload_xls/',Upload_xls,name="upload_xls")
]