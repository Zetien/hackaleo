from django.db import models


# Create your models here.
class SalesRecords(models.Model):
    region = models.CharField(max_length=250)
    country = models.CharField(max_length=50)
    item_type = models.CharField(max_length=50)
    sales_chanel = models.CharField(max_length=30)
    order_priority = models.CharField(max_length=10)
    order_date = models.DateField()
    order_id = models.IntegerField()
    ship_date = models.DateField()
    units_sold = models.IntegerField()
    unit_price = models.FloatField()
    unit_cost = models.FloatField()
    total_revenue = models.FloatField()
    total_cost = models.FloatField()
    total_profit = models.FloatField()

    def __str__(self):
        return (self.item_type)




class FileCsv(models.Model):
    date = models.DateField()
    description = models.CharField(max_length=120)
    deposits = models.FloatField()
    withdrawls = models.FloatField()
    balance = models.FloatField()


class Document(models.Model):
    title = models.CharField(max_length=150)
    uploadedFile = models.FileField(upload_to="upload_file/")
