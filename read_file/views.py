from django.shortcuts import render, redirect

# Create your views here.
from read_file.models import SalesRecords, FileCsv
import csv
import openpyxl
import pathlib
from pprint import pprint
from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
from . import models
from .models import Document, FileCsv
from datetime import datetime
from util.date_format_xls import filter_data


class ReadFile:

    def __init__(self, path):
        self.path = path

    def read_file(self):
        wb_obj = openpyxl.load_workbook(self.path)
        sheet_obj = wb_obj.active
        first_row = []

        for col in range(sheet_obj.max_column):
            first_row.append(sheet_obj.cell(1, col + 1).value)
        data = []

        for row in range(2, sheet_obj.max_row + 1):
            elm = {}
            for col in range(sheet_obj.max_column):
                elm[first_row[col]] = sheet_obj.cell(row, col + 1).value
            data.append(elm)
        return data


def Upload_csv(request):
    msg = ''
    documents = models.Document.objects.all()
    if request.method == 'POST':
        ext = ['.csv' ]
        title_file = request.POST['titleFile']
        upload_file = request.FILES['uploadFile']

        extension = pathlib.Path(upload_file.name).suffix

        if extension in ext:
            document = models.Document(
                title=title_file,
                uploadedFile=upload_file
            )
            document.save()

            documents = models.Document.objects.all()
            # return redirect('upload')

        else:
            msg = 'Archivo no compatible'
            documents = models.Document.objects.all()

        if document:
            data = []
            with open(document.uploadedFile.path, newline='') as file:
                reader = csv.DictReader(file)
                for row in reader:
                    data.append(row)
            for row in data:
                """date = row['Date'].split('-')[::-1]
                if date[1].isalpha():
                    pass
                else:
                    date_ref = datetime.strptime(row['Date'], '%d-%b-%Y')
                    date=date_ref
                    print(date_ref)"""
                date = datetime.strptime(row['Date'], '%d-%b-%Y')
                x=FileCsv(
                date =date.strftime('%Y-%m-%d'),
                description =row['Description'],
                deposits =tofloat(row['Deposits']),
                withdrawls =tofloat(row['Withdrawls']),
                balance =tofloat(row['Balance']),
                )
                x.save()

    return render(request, "upload_csv.html", context={'files': documents, 'msg': msg})


def Upload_xls(request):
    msg = ''
    documents = models.Document.objects.all()
    if request.method == 'POST':
        ext = ['.xlsx','.xls']
        title_file = request.POST['titleFile']
        upload_file = request.FILES['uploadFile']

        extension = pathlib.Path(upload_file.name).suffix

        if extension in ext:
            document = models.Document(
                title=title_file,
                uploadedFile=upload_file
            )
            document.save()

            if document:


                xls = filter_data(document.uploadedFile.path)

                for xl in xls:
                    data = SalesRecords(
                        region=xl['Region'],
                        country=xl['Country'],
                        item_type=xl['Item Type'],
                        sales_chanel=xl['Sales Channel'],
                        order_priority=xl['Order Priority'],
                        order_date=xl['Order Date'],
                        order_id=xl['Order ID'],
                        ship_date=xl['Ship Date'],
                        units_sold=xl['Units Sold'],
                        unit_price=xl['Unit Price'],
                        unit_cost=xl['Unit Cost'],
                        total_revenue=xl['Total Revenue'],
                        total_cost=xl['Total Cost'],
                        total_profit=xl['Total Profit'],
                        )
                    data.save()

            # return redirect('upload')

        else:
            msg = 'Archivo no compatible'
            documents = models.Document.objects.all()
    #x = data.objects.count()



    return render(request, "upload_xls.html", context={'files': documents, 'msg': msg})


class lisSalesRecords(ListView):

    model = SalesRecords
    queryset = SalesRecords.objects.all()
    template_name = 'lis_sales.html'
    context_object_name = 'lis_sales'


class lisFileCsv(ListView):
    
    model = FileCsv
    queryset = FileCsv.objects.all()
    template_name = 'lis_csv.html'
    context_object_name = 'lis_csv'


def tofloat(string=None):
    if isinstance(string, str):
        ##string = string.replace('.', '')
        string = string.replace(',', '')
    # float(string.strip().strip("'"))
    return float(string)

def read_file(path=None):
        wb_obj = openpyxl.load_workbook(path)
        sheet_obj = wb_obj.active
        first_row = []

        for col in range(sheet_obj.max_column):
            first_row.append(sheet_obj.cell(1, col + 1).value)
        data = []

        for row in range(2, sheet_obj.max_row + 1):
            elm = {}
            for col in range(sheet_obj.max_column):
                elm[first_row[col]] = sheet_obj.cell(row, col + 1).value
            data.append(elm)
        return data


class consultas(ListView):
    model = SalesRecords
    num_rows = SalesRecords.objects.all().count()
    #i = SalesRecords.objects.all().extra('select*from read_file_salesrecords')
    
