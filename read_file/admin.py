from django.contrib import admin

# Register your models here.

from read_file.models import SalesRecords, FileCsv, Document


@admin.register(SalesRecords)
class SalesRecordsAdmin(admin.ModelAdmin):
    pass


admin.site.register(FileCsv)


@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    pass